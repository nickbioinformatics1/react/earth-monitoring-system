import HomePage from './components/HomePage';
import Sidebar from './components/Sidebar/Sidebar';

function App() {
  return (
    <>
      <Sidebar />
      <HomePage />
    </>
  );
}

export default App;
