import React, { Suspense, useState } from 'react';

import { useControls } from 'leva';
import { Canvas } from 'react-three-fiber';
import SkyBox from './PlanetModel/SkyBox';
import CameraControls from './PlanetModel/CameraControls';
import Earth from './PlanetModel/Planets/Earth';
import Mars from './PlanetModel/Planets/Mars';
import './PlanetModel/css/planetmodel.css';

import Menu from './Menu/Menu';
import IssTracker from './Menu/IssTracker';
import styled from "styled-components";
import NearEarthObjects from './Menu/NearEarthObjects';

const HomePage = () => {
    const planets = ['earth','mars']
    const [selectedPlanet, setSelectedPlanet] = useState([{'planet':'earth'}]);
    const { a } = useControls({planet :{planet:'earth', options: planets, onChange: (v) => {setSelectedPlanet(v)}}});

    const CanvasContainer = styled.div`
        width: 100%;
        height: 100%;
        z-index: 50;
    `;

    return(
        <>
            <CanvasContainer>
                <Menu />
                <IssTracker />
                <NearEarthObjects />
                <Canvas>
                    <CameraControls />
                    <directionalLight intensity={1} />
                    <ambientLight intensity={0.9} />
                    <Suspense fallback='loading'>
                        {selectedPlanet === 'earth' ? (<Earth />) : selectedPlanet === 'mars' ? (<Mars />) : (<Earth />)}
                    </Suspense>
                    <SkyBox />
                </Canvas>
            </CanvasContainer>
        </>
    )
}

export default HomePage;