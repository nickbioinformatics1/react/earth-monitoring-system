import React, {useState} from 'react';
import styled from "styled-components";

const CanvasContainer = styled.div`
    width: 100%;
    height: 100%;
    background-color: black;
    position: absolute;
    z-index: 51;
    left: 10vh;
    display: flex;
`;

const Observatory = () => {
    return (
        <CanvasContainer>
            <p>This is observatory Page</p>
        </CanvasContainer>
    )
}

export default Observatory;