import React, {useState} from 'react';
import { BrowserRouter, Routes, Route, NavLink } from 'react-router-dom';

import HomePage from '../HomePage';
import Observatory from '../Observatory/Observatory';

import { FaHome, FaFortAwesome } from "react-icons/fa";
import styled from "styled-components";

const SideBarDiv = styled.div`
    align-items: left;
    background-color: transparent;
    flex-direction: column;
    height: 100%;
    position: absolute;
    z-index: 99;
    padding-top: 2vh;
    padding-left: 1vh;
    padding-right: 1vh;
    box-shadow: rgba(240, 46, 170, 0.4) 5px 5px, rgba(240, 46, 170, 0.3) 10px 10px, rgba(240, 46, 170, 0.2) 15px 15px, rgba(240, 46, 170, 0.1) 20px 20px, rgba(240, 46, 170, 0.05) 25px 25px;
    `;

const MenuDiv = styled.div`
    background-color: transparent;
`;

const MenuItem = styled.div`
    color: white;
    padding-top: 1vh;
`;

const SideBar = () => {
    const [hovered, setHovered] = useState(false);

    const SideBarItems = [
        {
            path: "/",
            name: "Home",
            icon: <FaHome size={24}/>,
        },
        {
            path: "/observatory",
            name: "Observatory",
            icon: <FaFortAwesome size={24}/>,
        }
    ]

    return (
        <BrowserRouter>
            <SideBarDiv onMouseEnter={() => setHovered(true)} onMouseLeave={() => setHovered(false)}>
                {
                    SideBarItems.map((item, index)=>(
                        <NavLink to={item.path} key={index} className="link" activeclassName="active" >
                            <MenuItem>{item.icon}</MenuItem>
                            {hovered ? <MenuItem style={{display: "flex"}}>{item.name}</MenuItem>:null}
                        </NavLink>
                    ))
                }
            </SideBarDiv>
            <Routes>
                <Route exact path="/" />
                <Route exact path="/observatory" element={<Observatory/>} />
            </Routes>
        </BrowserRouter>
    )
}

export default SideBar;