import { useEffect, useState } from "react";
import styled from "styled-components";

const SectionContainer = styled.div`
    align-items: center;
    border-radius: 8px;
    display: flex;
    flex-direction: column;
    height: auto;
    left: 10vh;
    opacity: 70%;
    padding-top: 1vh;
    padding-left: 1vh;
    position: relative;
    resize: both;
    top: 6vh;
    width: 20%;
    z-index: 50;
    box-shadow: rgba(240, 46, 170, 0.4) 5px 5px, rgba(240, 46, 170, 0.3) 10px 10px, rgba(240, 46, 170, 0.2) 15px 15px, rgba(240, 46, 170, 0.1) 20px 20px, rgba(240, 46, 170, 0.05) 25px 25px;
    `;

const Title = styled.h1`
    color: rgb(168, 168, 227);
    display: flex;
    text-align: center;
    font-weight: 800;
    `;

const TitleTwo = styled.h1`
    color: rgb(215, 166, 216);
    display: flex;
    text-align: center;
    font-size: 25px;
    font-weight: 500;
    `;

const Image = styled.img`
    border-radius: 8px;
    display: flex;
    height: 20vh;
    margin-top: 0vh;
    width:30vh;
    `;

const Details = styled.p`
    color: rgb(215, 166, 216);
    display: flex;
    font-weight: 500;
    padding-right: 1vh;
    text-align: justify;
    `;

const NasaDailyImage = () => {
    const [imageData, setImageData] = useState();
    const [openState, setOpenState] = useState(false);
    
    useEffect(() => {
        fetch('https://api.nasa.gov/planetary/apod?api_key=uraLJZmfQ5tdvRLNdRhHOGUbmsfvkp7Li247Ypup')
        .then((resp) => resp.json())
        .then((data) => {
            setImageData(data)
        })
        .catch((err) => {
            console.log(err.message)
        });;
    },[])

    const togglePanel = (e) => {
        setOpenState(!openState)
    };

    return (
            <SectionContainer onClick={togglePanel}>
                <Title>Image of the Day</Title>
                {openState? (
                    <>
                        <TitleTwo>{!imageData ? "Loading Data" : imageData.title}</TitleTwo>
                        <Image src ={!imageData ? "Loading Data": imageData.url} />
                        <Details>{!imageData ? "Loading Data" : imageData.explanation}</Details>
                    </>
                ) : null}
            </SectionContainer>
    )
}

export default NasaDailyImage;