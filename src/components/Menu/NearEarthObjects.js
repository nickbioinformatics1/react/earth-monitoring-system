import React, {Component} from 'react';
import styled from "styled-components";
import Plot from 'react-plotly.js';

const Plotdiv = styled.div`
    align-items: center;
    background-color: transparent;
    box-shadow: rgba(240, 46, 170, 0.4) 5px 5px, rgba(240, 46, 170, 0.3) 10px 10px, rgba(240, 46, 170, 0.2) 15px 15px, rgba(240, 46, 170, 0.1) 20px 20px, rgba(240, 46, 170, 0.05) 25px 25px;
    display: flex;
    height: auto;
    position: absolute;
    right: 2vh;
    top: 50vh;
    z-index: 50;
`

class NearEarthObjects extends Component {
    constructor(props) {
        super(props);
        this.state={
            data:[],
            date:[],
        }
    }

    componentDidMount() {
        var today = new Date();
        let day = ('0' + today.getDate()).slice(-2);
        let month = (today.getMonth()+1).toString().padStart(2,"0");
        let year = today.getFullYear();
        let currentDate = (year + '-' + month + '-' + day)
        const endpoint = 'https://api.nasa.gov/neo/rest/v1/feed?start_date=' + currentDate +'&end_date=' + currentDate + '&api_key=uraLJZmfQ5tdvRLNdRhHOGUbmsfvkp7Li247Ypup'

        fetch(endpoint)
            .then(reponse => reponse.json())
            .then(data => {
                this.setState({data: data})
                this.setState({date: currentDate})
            })
    }

    handleData (date, data) {
        let plot_data = [];
        let size = [];
        let id = [];
        let x = [];
        let y = [];
        let brightness = [];


         Object.keys(data).map(keyName => {
            Object.entries(data[keyName]).map(([element,value]) => {
                if(element===date) {
                    value.map(data => {
                        id.push(
                            'astroid_id: ' + data.id + '<br>' +
                            'astroid_name: ' + data.name + '<br>' +
                            'isThreat: ' + data.is_potentially_hazardous_asteroid +'<br>' +
                            'astroid brightness: ' + data.absolute_magnitude_h
                        )
                        size.push(data.estimated_diameter.meters.estimated_diameter_max/10)
                        data.close_approach_data.map(close => {
                            x.push(close.miss_distance.kilometers)
                            y.push(close.relative_velocity.kilometers_per_hour)
                        })
                    })
                }
            })
        })

        plot_data['x'] = x;
        plot_data['y'] = y;
        plot_data['size'] = size;
        plot_data['id'] = id;
        plot_data['brightness'] = brightness;
        return(plot_data)
    }

    render() {
        const plotData = this.handleData(this.state.date,this.state.data)
        return (
            <Plotdiv>
                <Plot
                    data = {[
                        {
                            type: 'scatter',
                            x: plotData['x'],
                            y: plotData['y'],
                            mode: 'markers',
                            marker: {
                                color: 'green',
                                size: plotData['size'],
                            },
                            text: plotData['id'],
                            hoverinfo: 'text',
                        }
                    ]}
                    layout={{
                        title: 'Hazardous Astroid Monitor',
                        font: {size: 20, color: 'white'},
                        xaxis: {title: 'Distance from earth (km)', titlefont: {color: 'white'}, size:10},
                        yaxis: {title: 'velocity (km/hr)', titlefont: {color: 'white'}, size:10},
                        autocolorscale: true,
                        fig_bgcolor: "rgb(255, 255, 255)",
                        plot_bgcolor: "rgba(0, 0, 0, 0)",
                        paper_bgcolor: "rgba(0, 0, 0, 0)"
                    }}
                />
            </Plotdiv>
        )
    }
}

export default NearEarthObjects;