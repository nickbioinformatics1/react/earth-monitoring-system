import React, { useState } from "react";
import NasaDailyImage from "./DailyNasaImage";
import styled from "styled-components";

const TopSectionContainer = styled.div`
    align-items: left;
    background-color: transparent;
    display: flex;
    flex-direction: column;
    height: 100%;
    padding-top: 2vh;
    padding-left: 2vh;
    position: absolute;
    width: 100%;
    z-index: 50;
    `;

const Logo = styled.h1`
    color: #fff;
    font-weight: 800;
    font-size: 50px;
    margin: 0;
  `;

const Menu = () => {
    return(
        <TopSectionContainer>
            {/* <Logo>The Space Dashboard</Logo> */}
            <NasaDailyImage />
        </TopSectionContainer>
    )
}

export default Menu