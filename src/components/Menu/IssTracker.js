import React, { Component } from "react";
import { ComposableMap, Geographies, Geography, Marker, ZoomableGroup } from "react-simple-maps";
import { FaHome } from "react-icons/fa";
import styled from "styled-components";

//const geoUrl = "https://raw.githubusercontent.com/deldersveld/topojson/master/world-countries.json"
const geoUrl = "https://cdn.jsdelivr.net/npm/world-atlas@2/countries-50m.json"

const IssContainer = styled.div`
    align-items: center;
    background-color: transparent;
    display: flex;
    flex-direction: column;
    height: auto;
    position: absolute;
    resize: both;
    top: 6vh;
    padding-top: 1vh;
    right: 2vh;
    width: 30%;
    height: 40%;
    z-index: 50;
    box-shadow: rgba(240, 46, 170, 0.4) 5px 5px, rgba(240, 46, 170, 0.3) 10px 10px, rgba(240, 46, 170, 0.2) 15px 15px, rgba(240, 46, 170, 0.1) 20px 20px, rgba(240, 46, 170, 0.05) 25px 25px;
    `;

const StyledGeography = styled(Geography)`
    fill: rgb(168, 168, 227);
  `;

const Title = styled.h1`
    color: rgb(168, 168, 227);
    display: flex;
    text-align: center;
    font-weight: 800;
    `;

const Details = styled.p`
    color: rgb(215, 166, 216);
    display: flex;
    font-weight: 500;
    padding-right: 1vh;
    text-align: justify;
    `;

class IssTracker extends Component {
    state= {
        lat: 0,
        lng: 0,
        zoom: 1,
        show_about: false,
    }

    componentDidMount() {
        this.interval = setInterval(this.getCoordinates,2000)
    }

    componentWillUnmount(){
        clearInterval(this.interval)
    }

    showAbout = (e) => {
        this.setState({show_about: true})
    }

    getCoordinates = async () => {
        fetch('https://api.wheretheiss.at/v1/satellites/25544')
            .then(res => res.json())
            .then(data => this.setState({
                lat: data.latitude,
                lng: data.longitude,
            })
        )
    }

    render() {
        return(
            <IssContainer>
                <Title>International Space Station Tracker</Title>
                <ComposableMap>
                    <ZoomableGroup center={[this.state.lng, this.state.lat]} zoom={this.state.zoom}>
                    <Geographies geography={geoUrl}>
                        {({ geographies }) =>
                        geographies.map((geo) => (
                            <StyledGeography key={geo.rsmKey} geography={geo}/>
                        ))}
                    </Geographies>
                    {<Marker coordinates={[this.state.lng, this.state.lat]} color="red">
                        <circle r={8} fill="red" />
                    </Marker>}
                    </ZoomableGroup>
                </ComposableMap>
                <Details>Latitude: {this.state.lat} ~ Longitude: {this.state.lng}</Details>
            </IssContainer>
        )
    }
}

export default IssTracker;