import { useThree } from 'react-three-fiber';
import { CubeTextureLoader } from 'three';
import front from './files/front.jpg';
import back from './files/back.jpg';
import top from './files/top.jpg';
import bottom from './files/bottom.jpg';
import left from './files/left.jpg';
import right from './files/right.jpg';

const SkyBox = () => {
    const { scene } = useThree();
    const loader = new CubeTextureLoader();

    const texture = loader.load([
        front,
        back,
        top,
        bottom,
        left, 
        right,
    ])

    scene.background = texture;
    return null;
};

export default SkyBox;