import React, {useEffect, useRef} from 'react';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { useFrame, useLoader } from 'react-three-fiber';
import earthGlbPath from '../files/Earth.glb';

const Earth = ({chosenPlanet}) => {
    const planet = useRef();
    useFrame(() => (planet.current.rotation.y += 0.00002));
    const { nodes } = useLoader(GLTFLoader, earthGlbPath);

    return (
        <mesh
            ref={planet}
            visible
            position={[0,0,0]}
            geometry={nodes.Cube001.geometry}
            material={nodes.Cube001.material}
        />
    );
};

export default Earth;