import React, {useEffect, useRef} from 'react';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { useFrame, useLoader } from 'react-three-fiber';
import marsGlbPath from '../files/Mars.glb';

const Mars = () => {
    const planet = useRef();
    useFrame(() => (planet.current.rotation.y += 0.00002));
    const { nodes } = useLoader(GLTFLoader, marsGlbPath);

    return (
        <mesh
            ref={planet}
            visible
            position={[0,0,0]}
            geometry={nodes.Cube008.geometry}
            material={nodes.Cube008.material}
        />
    );
};

export default Mars;